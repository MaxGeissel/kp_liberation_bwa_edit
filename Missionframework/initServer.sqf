if !isServer exitWith {};

#include "..\..\whitelist\public\public_whitelist.hpp"
#include "..\..\whitelist\whitelist.hpp"

publicVariable "BWA_stammgast_steamids";
publicVariable "BWA_stammspieler_steamids";
publicVariable "GRLIB_whitelisted_steamids";
publicVariable "BWA_TOC_id";
publicVariable "BWA_TOC_slots";
publicVariable "BWA_jetpilot_slots";
publicVariable "BWA_zugfuehrung_slots";
publicVariable "BWA_right_CASJET";
publicVariable "BWA_right_CASHelicopter";
publicVariable "BWA_CASJET";
publicVariable "BWA_CASHelicopter";


//Für Idioten ohne Fahrzeugrechte sind alle Fahrzeuge verboten
[
 "Car",
 "GetIn",
 {
	params ["_vehicle", "_role", "_unit", "_turret"];
	if !([_unit] call ace_common_fnc_isPlayer) exitWith {};
	//if !(local _unit) exitWith {};
	if (_role IN ["gunner", "turret"]) exitWith {
		if (count _turret > 0) then {
			_turret params ["_main", ["_second", -1]];
			if ((_main < 1) && (_second < 0)) then {
				if !([_unit, 0] call F_fetchPermission) exitWith {
					moveOut _unit;
					["Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
				};
			};
		};
	};
	if (_role IN ["driver", "commander"]) exitWith {
		if !([_unit, 0] call F_fetchPermission) exitWith {
			moveOut _unit;
			["Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
		};
	};
 },
 true,
 ["Tank"]
] call CBA_fnc_addClassEventHandler;

[
 "Car",
 "SeatSwitched",
 {
	 params ["_vehicle", "_unit1", "_unit2"];
	 if !([_unit1] call ace_common_fnc_isPlayer) exitWith {};
	 //if !(local _unit1) exitWith {};
	 _role = _unit1 call CBA_fnc_vehicleRole;
	 if (_role IN ["gunner", "turret"]) exitWith {
		 _fullcrew = (fullCrew _vehicle);
		 _index = (_fullcrew findIf {(_x select 0) isEqualTo _unit1});
		 if (_index >= 0) then {
			 (_fullcrew select _index) params ["_player", "_role", "_cargoIndex", "_turretPath", "isPerson"];
			 _turretPath params ["_main", ["_second", -1]];
			 if ((_main < 1) && (_second < 0)) then {
				 if !([_unit1, 0] call F_fetchPermission) exitWith {
					 moveOut _unit1;
					 _unit1 moveInCargo _vehicle;
					 ["Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
				 };
			 };
		 };
	 };
	 if (_role IN ["driver", "commander"]) exitWith {
		if !([_unit1, 0] call F_fetchPermission) exitWith {
			moveOut _unit1;
			_unit1 moveInCargo _vehicle;
			["Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
		};
	};
 },
 true,
 ["Tank"]
] call CBA_fnc_addClassEventHandler;

//Nur Gewhitelistete dürfen Panzerfahren
[
 "Tank",
 "GetIn",
 {
	params ["_vehicle", "_role", "_unit", "_turret"];
	if !([_unit] call ace_common_fnc_isPlayer) exitWith {};
	//if !(local _unit) exitWith {};
	if (_role IN ["gunner", "turret"]) exitWith {
		if (count _turret > 0) then {
			_turret params ["_main", ["_second", -1]];
			if ((_main < 1) && (_second < 0)) then {
				if !([_unit, 1] call F_fetchPermission) exitWith {
					moveOut _unit;
					["Schwere Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
				};
			};
		};
	};
	if (_role IN ["driver", "commander"]) exitWith {
		if !([_unit, 1] call F_fetchPermission) exitWith {
			moveOut _unit;
			["Schwere Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
		};
	};
 },
 true
] call CBA_fnc_addClassEventHandler;

[
 "Tank",
 "SeatSwitched",
 {
	 params ["_vehicle", "_unit1", "_unit2"];
	 if !([_unit1] call ace_common_fnc_isPlayer) exitWith {};
	 //if !(local _unit1) exitWith {};
	 _role = _unit1 call CBA_fnc_vehicleRole;
	 if (_role IN ["gunner", "turret"]) exitWith {
		 _fullcrew = (fullCrew _vehicle);
		 _index = (_fullcrew findIf {(_x select 0) isEqualTo _unit1});
		 if (_index >= 0) then {
			 (_fullcrew select _index) params ["_player", "_role", "_cargoIndex", "_turretPath", "isPerson"];
			 _turretPath params ["_main", ["_second", -1]];
			 if ((_main < 1) && (_second < 0)) then {
				 if !([_unit1, 1] call F_fetchPermission) exitWith {
					 moveOut _unit1;
					 _unit1 moveInCargo _vehicle;
					 ["Schwere Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
				 };
			 };
		 };
	 };
	 if (_role IN ["driver", "commander"]) exitWith {
		if !([_unit1, 1] call F_fetchPermission) exitWith {
			moveOut _unit1;
			_unit1 moveInCargo _vehicle;
			["Schwere Fahrzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
		};
	};
 },
 true
] call CBA_fnc_addClassEventHandler;



//Nur gewhitelistete Piloten dürfen fliegen etc
[
 "Air",
 "GetIn",
 {
	params ["_vehicle", "_role", "_unit", "_turret"];
	if !([_unit] call ace_common_fnc_isPlayer) exitWith {};
	//if !(local _unit) exitWith {};
	_typeOf = typeOf _vehicle;
	_uid = (getPlayerUID _unit);
	if (_role IN ["gunner", "turret"]) exitWith {
		if (count _turret > 0) then {
			_turret params ["_main", ["_second", -1]];
			if ((_main < 1) && (_second < 0)) then {
				if !([_unit, 2] call F_fetchPermission) exitWith {
					moveOut _unit;
					["Flugzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
				};
				//Prüfe, ob es ein CAS - Jet ist
				if (_typeOf isKindOf "Plane") then {
					if ({str _unit isEqualTo _x} count ["jpilot_1","jpilot_2"] < 1) then {
						["Dazu müssen Sie Pilot sein!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
						moveOut _unit;
					};
					if (({_X isEqualTo _typeOf} count BWA_CASJET > 0) && ({_x isEqualTo _uid} count (BWA_right_CASJET + BWA_TOC_id) < 1)) exitWith {
						moveOut _unit;
						["CAS-Jets zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
					};
				};
				//Prüfe, ob es ein CAS - Heli ist
				if (_typeOf isKindOf "Helicopter") then {
					if (({_X isEqualTo _typeOf} count BWA_CASHelicopter > 0) && ({_x isEqualTo _uid} count (BWA_right_CASHelicopter + BWA_TOC_id) < 1)) exitWith {
						moveOut _unit;
						["CAS-Chopper zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
					};
				};
			};
		};
	};
	if (_role IN ["driver", "commander"]) exitWith {
		if !([_unit, 2] call F_fetchPermission) exitWith {
			moveOut _unit;
			["Flugzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
		};
		//Prüfe, ob es ein CAS - Jet ist
		if (_typeOf isKindOf "Plane") then {
			if (({_X isEqualTo _typeOf} count BWA_CASJET > 0) && ({_x isEqualTo _uid} count (BWA_right_CASJET + BWA_TOC_id) < 1)) exitWith {
				moveOut _unit;
				["CAS-Jets zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
			};
		};
		//Prüfe, ob es ein CAS - Heli ist
		if (_typeOf isKindOf "Helicopter") then {
			if (({_X isEqualTo _typeOf} count BWA_CASHelicopter > 0) && ({_x isEqualTo _uid} count (BWA_right_CASHelicopter + BWA_TOC_id) < 1)) exitWith {
				moveOut _unit;
				["CAS-Chopper zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit];
			};
		};
	};
 },
 true,
 ["ParachuteBase"]
] call CBA_fnc_addClassEventHandler;

[
 "Air",
 "SeatSwitched",
 {
	 params ["_vehicle", "_unit1", "_unit2"];
	 if !([_unit1] call ace_common_fnc_isPlayer) exitWith {};
	 //if !(local _unit1) exitWith {};
	 _role = _unit1 call CBA_fnc_vehicleRole;
	 _typeOf = typeOf _vehicle;
	 _uid = (getPlayerUID _unit1);
	 if (_role IN ["gunner", "turret"]) exitWith {
		 _fullcrew = (fullCrew _vehicle);
		 _index = (_fullcrew findIf {(_x select 0) isEqualTo _unit1});
		 if (_index >= 0) then {
			 (_fullcrew select _index) params ["_player", "_role", "_cargoIndex", "_turretPath", "isPerson"];
			 _turretPath params ["_main", ["_second", -1]];
			 if ((_main < 1) && (_second < 0)) then {
				 if !([_unit1, 2] call F_fetchPermission) exitWith {
					 moveOut _unit1;
					 _unit1 moveInCargo _vehicle;
					 ["Flugzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
				 };
				 //Prüfe, ob es ein CAS - Jet ist
 				if (_typeOf isKindOf "Plane") then {
 					if (({_X isEqualTo _typeOf} count BWA_CASJET > 0) && ({_x isEqualTo _uid} count (BWA_right_CASJET + BWA_TOC_id) < 1)) exitWith {
 						moveOut _unit1;
						_unit1 moveInCargo _vehicle;
 						["CAS-Jets zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
 					};
 				};
 				//Prüfe, ob es ein CAS - Heli ist
 				if (_typeOf isKindOf "Helicopter") then {
 					if (({_X isEqualTo _typeOf} count BWA_CASHelicopter > 0) && ({_x isEqualTo _uid} count (BWA_right_CASHelicopter + BWA_TOC_id) < 1)) exitWith {
 						moveOut _unit1;
						_unit1 moveInCargo _vehicle;
 						["CAS-Chopper zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
 					};
 				};
			 };
		 };
	 };
	 if (_role IN ["driver", "commander"]) exitWith {
		if !([_unit1, 2] call F_fetchPermission) exitWith {
			moveOut _unit1;
			_unit1 moveInCargo _vehicle;
			["Flugzeuge zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
		};
		//Prüfe, ob es ein CAS - Jet ist
	 if (_typeOf isKindOf "Plane") then {
		 if (({_X isEqualTo _typeOf} count BWA_CASJET > 0) && ({_x isEqualTo _uid} count (BWA_right_CASJET + BWA_TOC_id) < 1)) exitWith {
			 moveOut _unit1;
			 _unit1 moveInCargo _vehicle;
			 ["CAS-Jets zu bedienen wurde Ihnen versagt!", true, 5, 1] remoteExecCall ["ace_common_fnc_displayText", _unit1];
		 };
	 };
	 //Prüfe, ob es ein CAS - Heli ist
	 if (_typeOf isKindOf "Helicopter") then {
		 if (({_X isEqualTo _typeOf} count BWA_CASHelicopter > 0) && ({_x isEqualTo _uid} count (BWA_right_CASHelicopter + BWA_TOC_id) < 1)) exitWith {
			 moveOut _unit1;
			 _unit1 moveInCargo _vehicle;
			 ["CAS-Chopper zu bedienen wurde Ihnen versagt!", true, 5, 1]  remoteExecCall ["ace_common_fnc_displayText", _unit1];
		 };
	 };
	};
 },
 true,
 ["ParachuteBase"]
] call CBA_fnc_addClassEventHandler;

[] execVM "scripts\bwa\slingfix.sqf";
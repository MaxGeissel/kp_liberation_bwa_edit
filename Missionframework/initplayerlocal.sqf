
//Frage ab, ob der Spieler slotberechtigt ist, sonst kick!
[
	{!(isNull player) && !(isNil "BWA_TOC_id")},
	{
		params ["_guid", "_player"];
		//Prüfe, ob der Spieler die Rechte für seinen Slot besitzt.
		switch (str _player) do {
			case "stelv_commandant";
			case "commandant" : {
				if ((({_guid isEqualTo _x} count (BWA_TOC_id + GRLIB_whitelisted_steamids + BWA_stammspieler_steamids)) < 1)) then {
					endMission "END1";
				};
			};
			case "sarzt";
			case "k_ZugF";
			case "Stelv_ZugF";
			case "Log_Zf" : {
				if (({_guid isEqualTo _x} count (BWA_TOC_id + GRLIB_whitelisted_steamids + BWA_stammspieler_steamids + BWA_stammgast_steamids)) < 1) then {
					endMission "END2";
				};
			};
			case "jpilot_1";
			case "jpilot_2" : {
				if !([_player, 2] call F_fetchPermission) then {
					endMission "END3";
				};
			};
			case "bwa_chaser";
			case "bwa_evil";
			case "bwa_max" : {
				if (({_guid isEqualTo _x} count BWA_TOC_id) < 1) then {
					endMission "ENDTOC";
				};
			};
			default {};
		};
	},
	[getPlayerUID player, player]
] call cba_fnc_waitUntilAndExecute;

#include "changelog.sqf"; //aktiviert Changelog
_box = _this;

//[_box, 0,["ACE_MainActions"]] call ace_interact_menu_fnc_removeActionFromObject;

[_box, 0, [], ["loadout","Ausrüsten","a3\ui_f\data\IGUI\Cfg\Actions\gear_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1 Heer
[_box, 0, ["loadout"], ["heer","Heer","a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Infanterie
[_box, 0, ["loadout","heer"], ["inf","Infanterie","a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe
[_box, 0, ["loadout","heer","inf"], ["jgrgrp","Jägergruppe","a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Gruppenführer
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrGrpFhr","Gruppenführer","a3\ui_f\data\gui\cfg\ranks\lieutenant_gs.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_GrpFhr.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Truppführer
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrTrpFhr","Truppführer","a3\ui_f\data\gui\cfg\ranks\sergeant_gs.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_GrpFhr.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Schütze
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrSchtz","Schütze","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_schtz.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Grenadier
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrGren","Grenadier","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_gren.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Kampfmittelbeseitiger
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrEod","Kampfmittelbeseitiger","a3\ui_f\data\map\vehicleicons\iconmanexplosive_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_eod.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Gruppenscharfschütze
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrGss","Gruppenscharfschütze","a3\ui_f\data\map\vehicleicons\iconmanrecon_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_gss.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | Einsatzersthelfer
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrEeh","Einsatzersthelfer","a3\ui_f\data\map\vehicleicons\iconmanmedic_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_eeh.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG3
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrMG3","MG 3","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG3.1
[_box, 0, ["loadout","heer","inf","jgrgrp","JgrMG3"], ["JgrMG3_1","MG 3 Schütze","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_mg3.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG3.2
[_box, 0, ["loadout","heer","inf","jgrgrp","JgrMG3"], ["JgrMG3_2","MG 3 Schütze 2","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_mg3_2.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG4
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrMG4","MG 4","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG4.1
[_box, 0, ["loadout","heer","inf","jgrgrp","JgrMG4"], ["JgrMG4_1","MG 4 Schütze","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_mg4.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG4.2
[_box, 0, ["loadout","heer","inf","jgrgrp","JgrMG4"], ["JgrMG4_2","MG 4 Schütze 2","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_mg4_2.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG5
[_box, 0, ["loadout","heer","inf","jgrgrp"], ["JgrMG5","MG 5","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG5.1
[_box, 0, ["loadout","heer","inf","jgrgrp","JgrMG5"], ["JgrMG5_1","MG 5 Schütze","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_mg5.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Jägergruppe | MG45.2
[_box, 0, ["loadout","heer","inf","jgrgrp","JgrMG5"], ["JgrMG5_2","MG 5 Schütze 2","a3\ui_f\data\map\vehicleicons\iconmanmg_ca.paa",{_player execVM "loadouts\fleck\inf\jgr\jgr_mg5_2.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

//================== ENDE Jägergruppe =======================

// 1.1 Heer | Infanterie | Fallschirmjäger
[_box, 0, ["loadout","heer","inf"], ["fschjgr","Fallschirmjäger","a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Gruppenführer
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrGrpFhr","Gruppenführer","a3\ui_f\data\gui\cfg\ranks\lieutenant_gs.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_GrpFhr.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Truppführer
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrTrpFhr","Truppführer","a3\ui_f\data\gui\cfg\ranks\sergeant_gs.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_GrpFhr.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Schütze
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrSchtz","Schütze","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_Schtz.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Einsatzersthelfer
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrEeh","Einsatzersthelfer","a3\ui_f\data\map\vehicleicons\iconmanmedic_ca.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_Eeh.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Grenadier
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrGren","Grenadier","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_Gren.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Kampfmittelbeseitiger
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrEOD","Kampfmittelbeseitiger","a3\ui_f\data\map\vehicleicons\iconmanexplosive_ca.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_EOD.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.1 Heer | Infanterie | Fallschirmjäger | Gruppenscharfschütze
[_box, 0, ["loadout","heer","inf","fschjgr"], ["FschJgrGSS","Gruppenscharfschütze","a3\ui_f\data\map\vehicleicons\iconmanrecon_ca.paa",{_player execVM "loadouts\fleck\inf\fschjgr\fschjgr_FKS.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

//================== ENDE Fallschirmjäger =======================

// 1.2 Heer | Spezialkräfte
[_box, 0, ["loadout","heer"], ["spez","Spezialkräfte","a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK
[_box, 0, ["loadout","heer","spez"], ["ksk","Kommando Spezialkräfte","a3\ui_f\data\IGUI\Cfg\Actions\getingunner_ca.paa",{}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | Gruppenführer
[_box, 0, ["loadout","heer","spez","ksk"], ["kskGrpFhr","Gruppenführer KSK","a3\ui_f\data\gui\cfg\ranks\captain_gs.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_GrpFhr.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | Truppführer
[_box, 0, ["loadout","heer","spez","ksk"], ["kskTrpFhr","Truppführer KSK","a3\ui_f\data\gui\cfg\ranks\lieutenant_gs.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_GrpFhr.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | Sanitäter
[_box, 0, ["loadout","heer","spez","ksk"], ["kskSan","Sanitäter KSK","a3\ui_f\data\map\vehicleicons\iconmanmedic_ca.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_eeh.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | Kampfmittelbeseitiger
[_box, 0, ["loadout","heer","spez","ksk"], ["kskEOD","Pionier KSK","a3\ui_f\data\map\vehicleicons\iconmanexplosive_ca.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_eod.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | UAV
//[_box, 0, ["loadout","heer","spez","ksk"], ["kwkUAV","UAV KSK (Darter)","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{[player] execVM "loadouts\fleck\spez\ksk\ksk_darter.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | Leiter
[_box, 0, ["loadout","heer","spez","ksk"], ["kskLeiter","Leiterträger KSK","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_leiter.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

// 1.2 Heer | Spezialkräfte | KSK | Gruppenscharfschütze
[_box, 0, ["loadout","heer","spez","ksk"], ["kskGss","Gruppenscharfschütze KSK","a3\ui_f\data\map\vehicleicons\iconmanrecon_ca.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_gss.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;

[_box, 0, ["loadout","heer","spez","ksk"], ["kskGren","Grenadier KSK","a3\ui_f\data\map\vehicleicons\iconman_ca.paa",{_player execVM "loadouts\fleck\spez\ksk\ksk_Gren.sqf";}, {true} , {}, [], [0,0,0], 10] call ace_interact_menu_fnc_createAction] call ace_interact_menu_fnc_addActionToObject;
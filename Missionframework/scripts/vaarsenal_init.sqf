[global_arsenal, true] call ace_arsenal_fnc_initBox;
sleep 30;
[global_arsenal, [
	// items
	"NVGogglesB_blk_F","NVGogglesB_grn_F","NVGogglesB_gry_F",
	"optic_Nightstalker","optic_tws","optic_tws_mg","optic_DMS_ghex_F","optic_LRPS_ghex_F",
	"optic_arco_ghex_f","optic_khs_hex",
	"rhsusf_acc_anpas13gv1",
	"G_Googles_VR",
	"Laserdesignator_02",
	"ACE_bodyBag",
	"ACE_SpraypaintBlack","ACE_SpraypaintBlue","ACE_SpraypaintGreen","ACE_SpraypaintRed",
	"ACE_key_civ","ACE_key_east","ACE_key_indp","ACE_key_lockpick","ACE_key_master","ACE_key_west",
	"U_B_Protagonist_VR","U_I_Protagonist_VR","U_O_Protagonist_VR",
	"rhsusf_hgu56p_mask_pink","rhsusf_hgu56p_pink","rhsusf_hgu56p_visor_mask_pink",
	"rhsusf_hgu56p_visor_pink","G_WirelessEarpiece_F","G_Respirator_white_F","G_Respirator_yellow_F",
	"G_Respirator_blue_F","G_EyeProtectors_F","G_EyeProtectors_Earpiece_F","G_Tactical_Black",
	"G_Tactical_Clear","O_NVGoggles_ghex_F","O_NVGoggles_hex_F","O_NVGoggles_urb_F",
	"ACE_MX2A", "Laserdesignator_02_ghex_F","G_O_Diving","G_I_Diving",
	
	// Funkgeräte
	"tf_anprc148jem","tf_anprc154","tf_fadak","ItemRadio","tf_pnr1000a","tf_rf7800str","PBW_sem52sl",
	"tf_mr3000_PBW_fleck","tf_mr3000_PBW_tropen", "tf_mr3000", "tf_mr3000_bwmod", "tf_mr3000_bwmod_tropen",
	"tf_mr3000_multicam", "tf_mr3000_rhs", "tf_mr6000l", "tf_anarc164", "tf_anprc155", "tf_anprc155_coyote",
	
	// Schalldämpfer
	"muzzle_snds_58_ghex_F","muzzle_snds_58_blk_F","muzzle_snds_58_hex_F","muzzle_snds_65_TI_ghex_F",
	"muzzle_snds_65_TI_blk_F","muzzle_snds_65_TI_hex_F",

	// Zweibeine
	"bipod_02_F_tan","bipod_02_F_hex","bipod_03_F_oli","bipod_03_F_blk","bipod_02_F_blk",
	
	// werfer
	"launch_B_Titan_F","launch_B_Titan_short_F","launch_B_Titan_short_tna_F","launch_B_Titan_tna_F",
	"launch_I_Titan_F","launch_I_Titan_short_F","launch_Titan_F","launch_Titan_short_F",
	"launch_O_Titan_F","launch_O_Titan_ghex_F","launch_O_Titan_short_F","launch_O_Titan_short_ghex_F",
	"launch_RPG32_F","launch_RPG32_ghex_F","launch_RPG7_F","launch_NLAW_F",
	
	// Terminals
	"I_UavTerminal","O_UavTerminal","C_UavTerminal",
	
	// Gewehre
	"arifle_CTARS_ghex_F","arifle_CTARS_hex_F","arifle_ARX_ghex_F","arifle_ARX_hex_F",
	"srifle_DMR_05_hex_F","srifle_GM6_ghex_F",
	"MMG_01_hex_F","srifle_DMR_07_ghex_F","srifle_DMR_07_hex_F","arifle_CTAR_ghex_F",
	"arifle_CTAR_hex_F","arifle_CTAR_GL_ghex_F","arifle_CTARS_hex_F","arifle_AK12_F","arifle_AK12_GL_F",
	"arifle_AKM_F","arifle_AKS_F","srifle_DMR_04_Tan_F","srifle_DMR_04_F","srifle_DMR_05_tan_f",
	"srifle_DMR_05_blk_F","MMG_01_tan_F","MMG_02_camo_F","MMG_02_sand_F","MMG_02_black_F",
	"arifle_CTAR_GL_hex_F","srifle_DMR_02_sniper_F","srifle_DMR_02_F","srifle_DMR_02_camo_F",

	// backpacks
	"B_Respawn_Sleeping_bag_blue_F","B_Respawn_Sleeping_bag_brown_F","B_Respawn_Sleeping_bag_F",
	"B_Respawn_TentA_F","B_Respawn_TentDome_F", //respawn Tents
	"ACRE_testBag","I_UAV_06_backpack_F","O_UAV_06_backpack_F","I_UAV_06_medical_backpack_F",
	"O_UAV_06_medical_backpack_F",
	"C_IDAP_UAV_06_medical_backpack_F","I_UAV_01_backpack_F","O_UAV_01_backpack_F",
	"C_IDAP_UAV_06_backpack_F","C_IDAP_UAV_01_backpack_F",
	"C_IDAP_UAV_06_antimine_backpack_F","tf_bussole",
	"I_GMG_01_A_weapon_F","O_GMG_01_A_weapon_F","B_GMG_01_A_weapon_F","I_HMG_01_A_weapon_F",
	"O_HMG_01_A_weapon_F","B_HMG_01_A_weapon_F",
	"I_HMG_01_weapon_F","O_HMG_01_weapon_F","B_HMG_01_weapon_F","I_HMG_01_high_weapon_F",
	"O_HMG_01_high_weapon_F","B_HMG_01_high_weapon_F",
	"I_GMG_01_high_weapon_F","O_GMG_01_high_weapon_F","B_GMG_01_high_weapon_F","I_GMG_01_weapon_F",
	"O_GMG_01_weapon_F","B_GMG_01_weapon_F",
	"I_HMG_01_support_high_F","O_HMG_01_support_high_F","B_HMG_01_support_high_F","I_HMG_01_support_F",
	"O_HMG_01_support_F","B_HMG_01_support_F",
	"B_FieldPack_ocamo","O_Static_Designator_02_weapon_F","B_Carryall_ocamo","I_Mortar_01_weapon_F",
	"O_Mortar_01_weapon_F","B_Mortar_01_weapon_F",
	"I_Mortar_01_support_weapon_F","O_Mortar_01_support_weapon_F","B_Mortar_01_support_weapon_F",
	"I_AA_01_weapon_F","O_AA_01_weapon_F","I_AT_01_weapon_F","O_AT_01_weapon_F","B_TacticalPack_ocamo",
	"B_ViperHarness_hex_F","B_ViperLightHarness_hex_F","B_Messenger_Gray_F","B_Messenger_Coyote_F",
	"B_Messenger_Olive_F","B_Messenger_Black_F","B_Messenger_IDAP_F",
	"B_Static_Designator_01_weapon_F","I_Mortar_01_support_F","O_Mortar_01_support_F",
	"B_Mortar_01_support_F","RHS_M2_Gun_Bag","RHS_M2_Tripod_Bag","rhs_M252_Gun_Bag",
	"rhs_M252_Bipod_Bag","RHS_M2_MiniTripod_Bag","RHS_Mk19_Gun_Bag","RHS_Mk19_Tripod_Bag",
	"B_AA_01_weapon_F","B_AT_01_weapon_F","rhs_Tow_Gun_Bag","rhs_TOW_Tripod_Bag",
	"B_UAV_06_medical_backpack_F","C_UAV_06_backpack_F","C_UAV_06_medical_backpack_F",
	"B_FieldPack_ghex_F","B_Patrol_Respawn_bag_F","B_Carryall_ghex_F","B_AssaultPack_ocamo","B_Bergen_hex_F",
	"V_RebratherIA","B_ViperLightHarness_ghex_F","B_ViperHarness_ghex_F",
	
	// Westen
	"V_TacVest_gen_F","V_Press_F","V_RebreatherIR","V_RebreatherAI","V_DeckCrew_blue_F",
	"V_DeckCrew_brown_F","V_TacVest_blk_POLICE",
	"V_DeckCrew_yellow_F","V_DeckCrew_green_F","V_DeckCrew_red_F","V_DeckCrew_violet_F",
	"V_DeckCrew_white_F","V_EOD_blue_F","V_EOD_IDAP_blue_F","V_Plain_crystal_F","V_Plain_medical_F",
	"V_Safety_blue_F","V_Safety_yellow_F","V_Safety_orange_F","V_Pocketed_coyote_F",
	"V_Pocketed_olive_F","V_Pocketed_black_F",
	
	//Uniformen
	"U_C_IDAP_Man_cargo_F","U_C_IDAP_Man_Jeans_F","U_C_IDAP_Man_casual_F","U_C_IDAP_Man_shorts_F",
	"U_C_IDAP_Man_Tee_F","U_C_IDAP_Man_TeeShorts_F","U_I_C_Soldier_Bandit_4_F",
	"U_I_C_Soldier_Bandit_5_F","U_I_C_Soldier_Bandit_1_F","U_I_C_Soldier_Bandit_3_F",
	"U_I_C_Soldier_Bandit_2_F","U_I_C_Soldier_Bandit_4_F","U_C_ConstructionCoverall_Blue_F",
	"U_C_ConstructionCoverall_Red_F","U_C_ConstructionCoverall_Black_F",
	"U_C_ConstructionCoverall_Vrana_F","U_C_Poloshiert_blue","U_C_Poloshiert_burgundy",
	"U_C_Poloshiert_salmon","U_C_Poloshiert_redwhite","U_C_Poloshiert_stripped",
	"U_C_Poloshiert_tricolour","U_C_Poloshiert_blue","U_C_Driver_1_blue","U_C_Driver_2","U_C_Driver_1",
	"U_C_Driver_1_yellow","U_C_Driver_1_green","U_C_Driver_1_orange","U_C_Driver_3","U_C_Driver_1_red",
	"U_C_Driver_1_black","U_C_Driver_4","U_C_Driver_1_white","U_B_GEN_Commander_F","U_B_GEN_Soldier_F",
	"U_C_Poor_1","U_O_T_Sniper_F","U_I_GhillieSuit","U_O_GhillieSuit","U_BG_leader",
	"U_I_HeliPilotCoveralls","U_OrestesBody","U_C_hunterBody_grn","U_C_Journalist","U_I_CombatUniform",
	"U_I_CombatUniform_shortsleeve","U_I_OfficerUniform","U_O_T_FullGhillie_tna_F",
	"U_C_Man_casual_2_F","U_C_Man_casual_3_F","U_C_Man_casual_1_F","U_O_officer_noinsignia_hex_F",
	"U_Marshal","U_C_Mechanic_01_F","U_O_CombatUniform_ocamo","U_O_CombatUniform_oucamo",
	"U_O_OfficerUniform_ocamo","U_O_T_Officer_F","U_I_C_Soldier_Para_2_F","U_I_C_Soldier_Para_4_F",
	"U_I_C_Soldier_Para_3_F","U_I_C_Soldier_Para_5_F","U_I_C_Soldier_Para_1_F","U_O_PilotCoveralls",
	"U_O_SpecopsUniform_ocamo","U_C_Paramedic_01_F","U_Rangemaster","U_C_Man_casual_5_F",
	"U_C_Man_casual_4_F","U_C_Man_casual_6_F","U_O_V_Soldier_Viper_F","U_O_V_Soldier_Viper_hex_F",
	"U_C_man_sport_3_F","U_C_man_sport_2_F","U_C_man_sport_1_F","U_I_C_Soldier_Camo_F",
	"U_O_T_Soldier_F","U_I_Wetsuit","U_O_Wetsuit","U_I_FullGhillie_Ish","U_O_FullGhillie_Ish",
	"U_I_FullGhillie_sard","U_O_FullGhillie_sard","U_I_FullGhillie_ard","U_O_FullGhillie_ard",
	"U_C_Protagonist_VR","U_Competitor","U_C_Scientist","U_I_pilotCoveralls","U_I_FullGhillie_Ish",
	"U_O_FUllGhillie_Ish","U_B_CTRG_Soldier_urb_1_F","U_B_CTRG_Soldier_urb_3_F","U_B_CTRG_Soldier_urb_2_F",
	"U_B_CTRG_Soldier_F","U_B_CTRG_Soldier_3_F","U_B_CTRG_Soldier_2_F",
			
	//Helme
	"H_HelmetSpecO_ocamo","rhs_xmas_antlers","H_HelmetCrew_O_ghex_F","H_HelmetCrew_I","H_HelmetCrew_O",
	"H_CrewHelmetHeli_I","H_CrewHelmetHeli_O",
	"H_PilotHelmetHeli_I","H_PilotHelmetHeli_O","H_PilotHelmetFighter_O","H_Cap_Orange_IDAP_F",
	"H_Cap_Black_IDAP_F","H_Cap_White_IDAP_F",
	"H_MilCap_gen_F","H_MilCap_ocamo","H_MilCap_dgtl","H_HelmetIA","H_Cap_police","H_Cap_press",
	"H_Cap_blk_Raven","H_Cap_brn_SPECOPS",
	"H_PilotHelmetFighter_I","H_PASGT_basic_blue_press_F","H_PASGT_neckprot_blue_press_F",
	"H_HelmetO_ViperSP_ghex_F","H_HelmetO_ViperSP_hex_F",
	"H_HelmetLeaderO_ghex_F","H_HelmetLeaderO_ocamo_F","H_HelmetLeaderO_oucamo_F","H_HelmetSpecO_blk",
	"H_Bandana_blu","H_Bandana_sand","H_Bandana_gry","H_Bandana_surfer_grn","H_Bandana_surfer_blk",
	"H_Bandana_camo","H_Beret_gen_F","H_Beret_02","H_Beret_Colonel","H_Beret_blk","H_Booniehat_dgtl",
	"H_HeadSet_yellow_F","H_HeadSet_orange_F","H_HeadSet_red_F","H_HeadSet_black_F","H_HeadSet_white_F"
	,"H_Hat_blue","H_Hat_brown","H_Hat_grey","H_Hat_tan","H_Hat_checker","H_Hat_camo",
	"H_WirelessEarpiece_F","H_HeadBandage_bloody_F","H_HeadBandage_stained_F","H_HeadBandage_clean_F",
	"H_Bandanna_cbr","H_Bandanna_sgg","H_Bandanna_khs_hs","H_Bandanna_khk","H_Bandanna_mcamo",
	"H_Bandanna_surfer","H_Cap_marshal","H_HelmetSpecO_ghex_F","H_MilCap_blue",
	"H_MilCap_gry","H_MilCap_ghex_F","H_MilCap_mcamo","H_MilCap_tna","H_Cap_grn_BI","H_Cap_blu",
	"H_Cap_blk_ION","H_Cap_oli_hs","H_Cap_red","H_Cap_surfer","H_EarProtectors_yellow_F",
	"H_EarProtectors_orange_F","H_EarProtectors_red_F","H_EarProtectors_black_F",
	"H_EarProtectors_white_F","H_RacingHelmet_1_blue_F","H_RacingHelmet_2_F","H_RacingHelmet_1_F",
	"H_RacingHelmet_1_yellow_F","H_RacingHelmet_1_green_F","H_RacingHelmet_1_orange_F",
	"H_RacingHelmet_3_F","H_RacingHelmet_1_red_F","H_RacingHelmet_1_black_F","H_RacingHelmet_4_F",
	"H_RacingHelmet_1_white_F","H_Hat_Safari_olive_F","H_Hat_Safari_sand_F","H_Hat_Safari_sand_F",
	"H_Cap_headphones","H_Construction_basic_yellow_F","H_Construction_headset_yellow_F",
	"H_Construction_earprot_yellow_F","H_HelmetO_ghex_F","H_HelmetO_ocamo","H_HelmetO_oucamo",
	"H_Construction_basic_orange_F","H_Construction_headset_orange_F","H_Construction_earprot_orange_F",
	"H_Construction_basic_red_F","H_Construction_headset_red_F","H_Construction_earprot_red_F",
	"H_Construction_basic_black_F","H_Construction_headset_black_F","H_Construction_earprot_black_F",
	"H_Construction_basic_vrana_F","H_Construction_headset_vrana_F","H_Construction_earprot_vrana_F",
	"H_Construction_basic_white_F","H_Construction_headset_white_F","H_Construction_earprot_white_F",
	"H_Helmet_Skate","H_StrawHat","H_StrawHat_dark","H_HelmetB_TI_tna_F","H_HelmetLeaderO_ocamo",
	"H_HelmetLeaderO_oucamo","H_Bandanna_surfer_grn","H_Bandanna_surfer_blk", "H_Tank_black_F",
	
	
	// Sprengstoffe
	"APERSMineDispenser_Mag",
	
	// Granaten
	"I_IR_Grenade","O_IR_Grenade",
	
	// magazines
	"BWA3_10Rnd_127x99_G82_Raufoss","BWA3_10Rnd_127x99_G82_Raufoss_Tracer",
	"BWA3_10Rnd_127x99_G82_Raufoss_Tracer_Dim","rhsusf_8Rnd_doomsday_Buck","Titan_AA","Titan_AT",
	"Titan_AP","10Rnd_127x54_Mag","10Rnd_93x64_DMR_05_Mag","150Rnd_93x64_Mag","130Rnd_338_Mag",
	"10Rnd_388_Mag","ACE_10Rnd_388_API526_Mag","ACE_10Rnd_388_300gr_HPBT_Mag"
	
	
	],true] call ace_arsenal_fnc_removeVirtualItems;
/*[global_arsenal, [



//////////////////
// RHS: USAF	//
//////////////////

// Gewehre
"rhs_weap_hk416d10
"rhs_weap_m14ebrri
"rhs_weap_m16a4
"rhs_weap_m16a4_carryhandle
"rhs_weap_m16a4_carryhandle_M203
"rhs_weap_m16a4_carryhandle_grip
"rhs_weap_m16a4_carryhandle_grip_pmag
"rhs_weap_m16a4_carryhandle_pmag
"rhs_weap_m16a4_grip
"rhs_weap_m27iar
"rhs_weap_m4
"rhs_weap_m4_carryhandle
"rhs_weap_m4_carryhandle_pmag
"rhs_weap_m4_grip
"rhs_weap_m4_grip2
"rhs_weap_m4_m203
"rhs_weap_m4_m203S
"rhs_weap_m4_m320
"rhs_weap_m4a1
"rhs_weap_m4a1_carryhandle
"rhs_weap_m4a1_carryhandle_grip
"rhs_weap_m4a1_carryhandle_grip2
"rhs_weap_m4a1_carryhandle_m203
"rhs_weap_m4a1_carryhandle_m203S
"rhs_weap_m4a1_carryhandle_pmag
"rhs_weap_m4a1_grip
"rhs_weap_m4a1_grip2
"rhs_weap_m4a1_m203
"rhs_weap_m4a1_m203s
"rhs_weap_m4a1_m320
"rhs_weap_sr25
"rhs_weap_sr25_ec
"rhs_weap_hk416d10
 

// Gewehr Munition
"rhsusf_20Rnd_762x51_m118_special_Mag
"rhsusf_20Rnd_762x51_m993_Mag
"30Rnd_556x45_Stanag
"30Rnd_556x45_Stanag_Tracer_Green
//"30Rnd_556x45_Stanag_Tracer_Red",
"30Rnd_556x45_Stanag_Tracer_Yellow
 

// Fernkampfwaffen
"rhs_weap_XM2010
"rhs_weap_XM2010_d
"rhs_weap_XM2010_sa
"rhs_weap_XM2010_wd
"rhs_weap_m107_d
"rhs_weap_m107_w
 

// Fernkamfwaffen Munition
"rhsusf_5Rnd_300winmag_xm2010
"10Rnd_RHS_50BMG_Box
"rhsusf_10Rnd_STD_50BMG_M107
 

// Leichte Maschinengewehre
"rhs_weap_m240B
"rhs_weap_m240B_CAP
"rhs_weap_m240G
"rhs_weap_m249_pip_L
"rhs_weap_m249_pip_L_para
"rhs_weap_m249_pip_L_vfg
"rhs_weap_m249_pip_S
"rhs_weap_m249_pip_S_para
"rhs_weap_m249_pip_S_vfg
 

// Leichte Maschinengewehr Munition
"rhs_200rnd_556x45_M_SAW
"rhs_200rnd_556x45_T_SAW
"rhs_200rnd_556x45_B_SAW
"rhsusf_50Rnd_762x51
"rhsusf_50Rnd_762x51_m61_ap
"rhsusf_50Rnd_762x51_m62_tracer
"rhsusf_50Rnd_762x51_m80a1epr
"rhsusf_100Rnd_762x51
"rhsusf_100Rnd_762x51_m61_ap
"rhsusf_100Rnd_762x51_m62_tracer
"rhsusf_100Rnd_762x51_m80a1epr
"rhsusf_50Rnd_762x51_m993
"rhsusf_100Rnd_762x51_m993
 

// Granatwerfer
"rhs_weap_M320
"rhs_weap_m32
 

// Granatwerfer Munition
"rhsusf_mag_6Rnd_M441_HE
"rhsusf_mag_6Rnd_M433_HEDP
"rhsusf_mag_6Rnd_M714_white
"rhsusf_mag_6Rnd_M576_Buckshot
"rhsusf_m112_mag
"rhsusf_m112x4_mag
"rhs_mag_m18_green
"rhs_mag_m18_purple
"rhs_mag_m18_red
"rhs_mag_m18_yellow
 

// Schrotflinten
"rhs_weap_M590_5RD
"rhs_weap_M590_8RD
 

// Schrotflinten Munition
"rhsusf_5Rnd_00Buck
"rhsusf_8Rnd_00Buck
"rhsusf_5Rnd_Slug
"rhsusf_8Rnd_Slug
"rhsusf_5Rnd_HE
"rhsusf_8Rnd_HE
"rhsusf_5Rnd_FRAG
"rhsusf_8Rnd_FRAG
"rhsusf_5Rnd_doomsday_Buck
"rhsusf_8Rnd_doomsday_Buck
 

// Werfer
"rhs_weap_M136
"rhs_weap_M136_hedp
"rhs_weap_M136_hp
"rhs_weap_fgm148
"rhs_weap_fim92
"rhs_weap_smaw
"rhs_weap_smaw_green
 

// Werfer Munition
"rhs_m136_mag
"rhs_m136_hedp_mag
"rhs_m136_hp_mag
"rhs_fim92_mag
"rhs_fgm148_magazine_AT
"rhs_mag_smaw_HEAA
"rhs_mag_smaw_HEDP
"rhs_mag_smaw_SR
 

// Pistolen
"rhsusf_weap_glock17g4
"rhsusf_weap_m1911a1
"rhsusf_weap_m9
 

// Pistolen Magaziene
"rhsusf_mag_7x45acp_MHP
"rhsusf_mag_17Rnd_9x19_FMJ
"rhsusf_mag_17Rnd_9x19_JHP
"rhsusf_mag_15Rnd_9x19_FMJ
"rhsusf_mag_15Rnd_9x19_JHP
 

// Waffen zubehör
"rhsusf_acc_harris_bipod
"rhs_acc_at4_handler
"rhsusf_acc_anpeq15A
"rhsusf_acc_anpeq15
"rhsusf_acc_anpeq15_light
"rhsusf_acc_M2010S
"rhsusf_acc_anpeq15side
"rhsusf_acc_SR25S
"rhsusf_acc_rotex5_grey
"rhsusf_acc_rotex5_tan
"rhsusf_acc_nt4_black
"rhsusf_acc_nt4_tan
"rhsusf_acc_muzzleFlash_SF
"rhsusf_acc_SF3P556
"rhsusf_acc_SFMB556
"rhsusf_acc_compm4
"rhsusf_acc_eotech_552
"rhsusf_acc_LEUPOLDMK4
"rhsusf_acc_M2A1
"rhsusf_acc_EOTECH
"rhsusf_acc_LEUPOLDMK4_2
"rhsusf_acc_ACOG3_USMC
"rhsusf_acc_ACOG2_USMC
"rhsusf_acc_ACOG_USMC
"rhsusf_acc_ACOG3
"rhsusf_acc_ACOG2
"rhsusf_acc_ACOG_pip
"rhsusf_acc_ACOG_sa
"rhsusf_acc_ACOG_d
"rhsusf_acc_ACOG_wd
"rhsusf_acc_ACOG
 

// Uniformen
rhs_uniform_FROG01_d
rhs_uniform_FROG01_m81
rhs_uniform_FROG01_wd
rhs_uniform_cu_ocp
rhs_uniform_cu_ocp_101st
rhs_uniform_cu_ocp_10th
rhs_uniform_cu_ocp_1stcav
rhs_uniform_cu_ocp_82nd
rhs_uniform_cu_ucp
rhs_uniform_cu_ucp_101st
rhs_uniform_cu_ucp_10th
rhs_uniform_cu_ucp_1stcav
rhs_uniform_cu_ucp_82nd
 

// Westen
rhsusf_iotv_ocp
rhsusf_iotv_ocp_Grenadier
rhsusf_iotv_ocp_Medic
rhsusf_iotv_ocp_Repair
rhsusf_iotv_ocp_Rifleman
rhsusf_iotv_ocp_SAW
rhsusf_iotv_ocp_Squadleader
rhsusf_iotv_ocp_Teamleader
rhsusf_iotv_ucp
rhsusf_iotv_ucp_Grenadier
rhsusf_iotv_ucp_Medic
rhsusf_iotv_ucp_Repair
rhsusf_iotv_ucp_Rifleman
rhsusf_iotv_ucp_SAW
rhsusf_iotv_ucp_Squadleader
rhsusf_iotv_ucp_Teamleader
rhsusf_spc
rhsusf_spc_corpsman
rhsusf_spc_crewman
rhsusf_spc_iar
rhsusf_spc_light
rhsusf_spc_marksman
rhsusf_spc_mg
rhsusf_spc_rifleman
rhsusf_spc_squadleader
rhsusf_spc_teamleader
 

// Rucksäcke
B_rhsusf_B_BACKPACK
rhsusf_assault_eagleaiii_coy
rhsusf_assault_eagleaiii_ocp
rhsusf_assault_eagleaiii_ucp
rhsusf_falconii
 

// Kopfbedeckung
rhs_Booniehat_m81
rhs_Booniehat_marpatd
rhs_Booniehat_marpatwd
rhs_Booniehat_ocp
rhs_Booniehat_ucp
rhsusf_Bowman
rhsusf_ach_bare
rhsusf_ach_bare_des
rhsusf_ach_bare_des_ess
rhsusf_ach_bare_des_headset
rhsusf_ach_bare_des_headset_ess
rhsusf_ach_bare_ess
rhsusf_ach_bare_headset
rhsusf_ach_bare_headset_ess
rhsusf_ach_bare_semi
rhsusf_ach_bare_semi_ess
rhsusf_ach_bare_semi_headset
rhsusf_ach_bare_semi_headset_ess
rhsusf_ach_bare_tan
rhsusf_ach_bare_tan_ess
rhsusf_ach_bare_tan_headset
rhsusf_ach_bare_tan_headset_ess
rhsusf_ach_bare_wood
rhsusf_ach_bare_wood_ess
rhsusf_ach_bare_wood_headset
rhsusf_ach_bare_wood_headset_ess
rhsusf_ach_helmet_ESS_ocp
rhsusf_ach_helmet_ESS_ucp
rhsusf_ach_helmet_M81
rhsusf_ach_helmet_camo_ocp
rhsusf_ach_helmet_headset_ess_ocp
rhsusf_ach_helmet_headset_ess_ucp
rhsusf_ach_helmet_headset_ocp
rhsusf_ach_helmet_headset_ucp
rhsusf_ach_helmet_ocp
rhsusf_ach_helmet_ocp_norotos
rhsusf_ach_helmet_ucp
rhsusf_ach_helmet_ucp_norotos
rhsusf_bowman_cap
rhsusf_lwh_helmet_M1942
rhsusf_lwh_helmet_marpatd
rhsusf_lwh_helmet_marpatd_ess
rhsusf_lwh_helmet_marpatd_headset
rhsusf_lwh_helmet_marpatwd
rhsusf_lwh_helmet_marpatwd_ess
rhsusf_lwh_helmet_marpatwd_headset
rhsusf_mich_bare
rhsusf_mich_bare_alt
rhsusf_mich_bare_alt_semi
rhsusf_mich_bare_alt_tan
rhsusf_mich_bare_headset
rhsusf_mich_bare_norotos
rhsusf_mich_bare_norotos_alt
rhsusf_mich_bare_norotos_alt_headset
rhsusf_mich_bare_norotos_alt_semi
rhsusf_mich_bare_norotos_alt_semi_headset
rhsusf_mich_bare_norotos_alt_tan
rhsusf_mich_bare_norotos_alt_tan_headset
rhsusf_mich_bare_norotos_arc
rhsusf_mich_bare_norotos_arc_alt
rhsusf_mich_bare_norotos_arc_alt_headset
rhsusf_mich_bare_norotos_arc_alt_semi
rhsusf_mich_bare_norotos_arc_alt_semi_headset
rhsusf_mich_bare_norotos_arc_alt_tan
rhsusf_mich_bare_norotos_arc_alt_tan_headset
rhsusf_mich_bare_norotos_arc_headset
rhsusf_mich_bare_norotos_arc_semi
rhsusf_mich_bare_norotos_arc_semi_headset
rhsusf_mich_bare_norotos_arc_tan
rhsusf_mich_bare_norotos_headset
rhsusf_mich_bare_norotos_semi
rhsusf_mich_bare_norotos_semi_headset
rhsusf_mich_bare_norotos_tan
rhsusf_mich_bare_norotos_tan_headset
rhsusf_mich_bare_semi
rhsusf_mich_bare_semi_headset
rhsusf_mich_bare_tan
rhsusf_mich_bare_tan_headset
rhsusf_mich_helmet_marpatdItemMap
rhsusf_mich_helmet_marpatd_altItemMap
rhsusf_mich_helmet_marpatd_alt_headset
rhsusf_mich_helmet_marpatd_headset
rhsusf_mich_helmet_marpatd_norotos
rhsusf_mich_helmet_marpatd_norotos_arc
rhsusf_mich_helmet_marpatd_norotos_arc_headset
rhsusf_mich_helmet_marpatd_norotos_headset
rhsusf_mich_helmet_marpatwd
rhsusf_mich_helmet_marpatwd_alt
rhsusf_mich_helmet_marpatwd_alt_headset
rhsusf_mich_helmet_marpatwd_headset
rhsusf_mich_helmet_marpatwd_norotos
rhsusf_mich_helmet_marpatwd_norotos_arc
rhsusf_mich_helmet_marpatwd_norotos_arc_headset
rhsusf_mich_helmet_marpatwd_norotos_headset
rhsusf_opscore_bk
rhsusf_opscore_coy_cover
rhsusf_opscore_coy_cover_pelt
rhsusf_opscore_fg
rhsusf_opscore_mc_cover
rhsusf_opscore_mc_cover_pelt
rhsusf_opscore_rg_cover
rhsusf_opscore_rg_cover_pelt
rhsusf_opscore_ut

// Explosiev
"rhsusf_m112_mag",
"rhsusf_m112x4_mag",



//////////////////
// ACE			//
//////////////////

// + Tools
"ACE_RangeTable_82mm",
"ACE_ATragMX",
"ACE_wirecutter",
"ACE_RangeCard",
"ACE_DefusalKit",
"ACE_Flashlight_MX991",
"ACE_HuntIR_monitor",
"ACE_IR_Strobe_Item",
"ACE_CableTie",
"ACE_MapTools",
"ACE_Kestrel4500",
"ACE_EntrenchingTool",
"ACE_Chemlight_Shield",
"ACE_Flashlight_KSF1",
"ACE_M26_Clacker",
"ACE_Clacker",
"ACE_Flashlight_XL50",
"ACE_microDAGR",
"ACE_Cellphone",
"ACE_EarPlugs",
"ACE_Sandbag_empty",
"ACE_Tripod",
"ACE_SpottingScope",
"ACE_DeadManSwitch",
"ACE_UAVBattery",

// Medizinisches
"ACE_adenosine",
"ACE_atropine",
"adv_aceCPR_AED",
"ACE_fieldDressing",
"ACE_elasticBandage",
"ACE_bloodIV",
"ACE_bloodIV_250",
"ACE_bloodIV_500",
"ACE_epinephrine",
"ACE_salineIV",
"ACE_salineIV_250",
"ACE_salineIV_500",
"ACE_bodyBag",
"ACE_morphine",
"ACE_packingBandage",
"ACE_surgicalKit",
"ACE_personalAidKit",
"ACE_plasmaIV",
"ACE_plasmaIV_250",
"ACE_plasmaIV_500",
"ACE_tourniquet",
"ACE_quikclot",

// Explosiev
"ACE_FlareTripMine_Mag",

// Granaten
"ACE_Chemlight_HiBlue",
"ACE_Chemlight_HiGreen",
"ACE_Chemlight_UltraHiOrange",
"ACE_Chemlight_HiYellow",
"ACE_Chemlight_IR",
"ACE_Chemlight_Orange",
"ACE_Chemlight_HiRed",
"ACE_Chemlight_White",
"ACE_Chemlight_HiWhite",
"ACE_HandFlare_Yellow",
"ACE_HandFlare_Green",
"ACE_HandFlare_Red",
"ACE_HandFlare_White",
"ACE_M84",



//////////////////
// BWMod		//
//////////////////

// + Tools

"BWA3_optic_IRV600","BWA3_optic_NSV600",
"BWA3_optic_NSV80",





//	"item1", "item2"
//////////////////
// Vanilla		//
//////////////////

// + Tools

"MineDetector",
"ToolKit",

// Expl.

"DemoCharge_Remote_Mag",
"SatchelCharge_Remote_Mag",
"ClaymoreDirectionalMine_Remote_Mag",

// Granaten
"BWA3_DM25","BWA3_DM32_Blue","BWA3_DM32_Yellow","BWA3_DM32_Green","BWA3_DM32_Orange","BWA3_DM32_Red","BWA3_DM32_Purple",
"BWA3_DM51A1","B_IR_Grenade","Chemlight_blue","Chemlight_yellow","Chemlight_green","Chemlight_red","HandGrenade","rhs_mag_m67","rhs_mag_m7a3_cs","rhs_mag_mk84",


// Satnav / Terminals
"B_UavTerminal","BWA3_ItemNaviPad",
"ItemGPS",

// ???
"ItemWatch","tf_microdagr","ACE_Altimeter",

// Funk / Komunikation
"tf_anprc154","tf_anprc152","tf_anprc148jem","PBW_sem52sl",

// KOmpass
"ItemCompass",

// Karte
"ItemMap",

// Ferngläser
"Laserdesignator",
"Laserdesignator_03","Laserdesignator_01_khk_F","rhsusf_bino_lerca_1200_black","rhsusf_bino_lerca_1200_tan","rhsusf_bino_leopold_mk4","BWA3_Vector",
"ACE_Yardage450","ACE_Vector","ACE_VectorDay","rhsusf_bino_lrf_Vector21",

// Nachtsicht
"rhsusf_ANPVS_14","rhsusf_ANPVS_15","ACE_NVG_Wide","ACE_NVG_Gen4","ACE_NVG_Gen2",
"ACE_NVG_Gen1","rhsusf_Rhino","rhs_googles_orange","rhs_googles_yellow","rhs_googles_clear","rhs_ess_black","rhs_googles_black","G_Bandanna_shades",
"G_Bandanna_beast","G_Bandanna_tan","G_Bandanna_khk","G_Bandanna_oli","G_Bandanna_aviator","G_Bandanna_blk","BWA3_G_Combat_clear","BWA3_G_Combat_orange",
"BWA3_G_Combat_black","G_Lady_Blue","G_Aviator","None","PBW_Headset","PBW_RevisionT_Dunkel","PBW_RevisionT_Klar","PBW_RevisionF_Dunkel","PBW_RevisionF_klar",
"rhs_scarf","PBW_shemagh_beige","PBW_shemagh_gruen","rhsusf_shemagh_grn","rhsusf_shemagh2_grn","rhsusf_shemagh_od","rhsusf_shemagh2_od","rhsusf_shemagh_tan",
"rhsusf_shemagh2_tan","rhsusf_shemagh_white","rhsusf_shemagh2_white","rhsusf_shemagh_gogg_grn","rhsusf_shemagh2_gogg_grn","rhsusf_shemagh_gogg_od",
"rhsusf_shemagh2_gogg_od","rhsusf_shemagh_gogg_tan","rhsusf_shemagh2_gogg_tan","rhsusf_shemagh_gogg_white","rhsusf_shemagh2_gogg_white",
"rhsusf_oakley_goggles_blk","rhsusf_oakley_goggles_clr","rhsusf_oakley_goggles_ylw","PBW_Balaclava_beige","PBW_Balaclava_beigeR","PBW_Balaclava_schwarzR",
"G_Balaclava_combat","G_Balaclava_lowprofile","G_Balaclava_oli","G_Balaclava_blk","PBW_Balaclava_schwarz","G_Tactical_Clear","G_Tactical_Black",
"G_Balaclava_TI_tna_F","G_Balaclava_TI_G_tna_F","G_Balaclava_TI_blk_F","G_Balaclava_TI_G_blk_F","G_Diving","G_B_Diving","G_Lowprofile",
"BWA3_AssaultPack_Fleck","BWA3_AssaultPack_Tropen","B_LegStrapBag_coyote_F","B_LegStrapBag_olive_F","B_LegStrapBag_black_F","B_Bergen_dgtl_F",
"B_Bergen_tna_F","rhs_d6_Parachute_backpack","rhsusf_assault_eagleaiii_coy","B_rhsusf_B_BACKPACK","rhsusf_assault_eagleaiii_ocp","rhsusf_assault_eagleaiii_ucp",
"rhsusf_falconii_coy","rhsusf_falconii_mc","rhsusf_falconii","BWA3_FieldPack_Fleck","BWA3_FieldPack_Tropen","B_Static_Designator_01_weapon_F",
"BWA3_Carryall_Fleck","BWA3_Carryall_Tropen","BWA3_PatrolPack_Fleck","BWA3_PatrolPack_Tropen","B_Mortar_01_weapon_F","B_Mortar_01_support_F",
"Redd_Tank_M120_Tampella_Tripod","Redd_Tank_M120_Tampella_Barrel","B_Respawn_TentA_F","B_Respawn_TentDome_F","B_Patrol_Respawn_bag_F","RHS_M2_MiniTripod_Bag",
"RHS_M2_Tripod_Bag","RHS_M2_Gun_Bag","rhs_medic_bag","Redd_Milan_Static_Barrel","Redd_Milan_Static_Tripod","tf_rt1523g_big_bwmod","tf_rt1523g_big_bwmod_tropen",
"tf_rt1523g","tf_rt1523g_big_rhs","tf_rt1523g_bwmod","tf_rt1523g_sage","tf_rt1523g_rhs","tf_rt1523g_green","tf_rt1523g_fabric","tf_rt1523g_black",
"tf_rt1523g_big","BWA3_Kitbag_Fleck_Medic","BWA3_Kitbag_Fleck","BWA3_Kitbag_Tropen","BWA3_Kitbag_Tropen_Medic","rhs_sidor","rhsusf_eject_Parachute_backpack",
"B_Parachute","BWA3_TacticalPack_Fleck","BWA3_TacticalPack_Fleck_Medic","BWA3_TacticalPack_Tropen","BWA3_TacticalPack_Tropen_Medic","ACE_TacticalLadder_Pack",
"rhs_Tow_Gun_Bag","rhs_TOW_Tripod_Bag","B_UAV_06_backpack_F","B_UAV_06_medical_backpack_F","B_UAV_01_backpack_F","C_IDAP_UAV_06_antimine_backpack_F",
"C_UAV_06_medical_backpack_F","C_UAV_06_backpack_F","ACE_NonSteerableParachute","B_AssaultPack_Kerry","ace_gunbag","ace_gunbag_Tan","B_ViperHarness_ghex_F",
"B_ViperHarness_khk_F","B_ViperHarness_oli_F","B_ViperHarness_blk_F","B_ViperHarness_hex_F","B_ViperLightHarness_ghex_F","B_ViperLightHarness_khk_F",
"B_ViperLightHarness_oli_F","B_ViperLightHarness_blk_F","B_ViperLightHarness_hex_F","rhs_assault_umbts_engineer_empty","rhs_assault_umbts","V_EOD_olive_F",
"BWA3_Vest_Fleck","BWA3_Vest_Grenadier_Fleck","BWA3_Vest_Leader_Fleck","BWA3_Vest_Marksman_Fleck","BWA3_Vest_MachineGunner_Fleck","BWA3_Vest_Medic_Fleck",
"BWA3_Vest_Rifleman_Fleck","BWA3_Vest_Tropen","BWA3_Vest_Grenadier_Tropen","BWA3_Vest_Leader_Tropen","BWA3_Vest_Marksman_Tropen",
"BWA3_Vest_MachineGunner_Tropen","BWA3_Vest_Medic_Tropen","BWA3_Vest_Rifleman_Tropen","rhsusf_iotv_ocp_Grenadier","rhsusf_iotv_ucp_Grenadier",
"rhsusf_iotv_ocp_Medic","rhsusf_iotv_ucp_Medic","rhsusf_iotv_ocp","rhsusf_iotv_ocp_Repair","rhsusf_iotv_ucp_Repair","rhsusf_iotv_ocp_Rifleman",
"rhsusf_iotv_ucp_Rifleman","rhsusf_iotv_ocp_SAW","rhsusf_iotv_ucp_SAW","rhsusf_iotv_ocp_Squadleader","rhsusf_iotv_ucp_Squadleader","rhsusf_iotv_ocp_Teamleader",
"rhsusf_iotv_ucp_Teamleader","rhsusf_iotv_ucp","BWA3_Vest_JPC_Radioman_Fleck","BWA3_Vest_JPC_Leader_Fleck","BWA3_Vest_JPC_Rifleman_Fleck",
"BWA3_Vest_JPC_Radioman_Tropen","BWA3_Vest_JPC_Leader_Tropen","BWA3_Vest_JPC_Rifleman_Tropen","rhs_vest_commander","pbw_koppel_schtz","pbw_koppel_sani",
"pbw_koppel_mg","pbw_koppel_mg_h","pbw_koppel_grpfhr","rhsusf_mbav","rhsusf_mbav_grenadier","rhsusf_mbav_light","rhsusf_mbav_mg","rhsusf_mbav_medic",
"rhsusf_mbav_rifleman","V_Pocketed_coyote_F","V_Pocketed_olive_F","V_Pocketed_black_F","V_TacVestIR_blk","V_Rangemaster_belt","rhsusf_spc",
"rhsusf_spc_corpsman","rhsusf_spc_crewman","rhsusf_spc_iar","rhsusf_spc_light","rhsusf_spc_mg","rhsusf_spc_marksman","rhsusf_spc_patchless",
"rhsusf_spc_patchless_radio","rhsusf_spc_rifleman","rhsusf_spc_squadleader","rhsusf_spc_teamleader","rhsusf_spcs_ocp_crewman","rhsusf_spcs_ucp_crewman",
"rhsusf_spcs_ocp_grenadier","rhsusf_spcs_ucp_grenadier","rhsusf_spcs_ocp_machinegunner","rhsusf_spcs_ucp_machinegunner","rhsusf_spcs_ocp_medic",
"rhsusf_spcs_ucp_medic","rhsusf_spcs_ocp","rhsusf_spcs_ocp_rifleman_alt","rhsusf_spcs_ucp_rifleman_alt","rhsusf_spcs_ocp_rifleman","rhsusf_spcs_ucp_rifleman",
"rhsusf_spcs_ocp_saw","rhsusf_spcs_ucp_saw","rhsusf_spcs_ocp_sniper","rhsusf_spcs_ucp_sniper","rhsusf_spcs_ocp_squadleader","rhsusf_spcs_ucp_squadleader",
"rhsusf_spcs_ocp_teamleader_alt","rhsusf_spcs_ucp_teamleader_alt","rhsusf_spcs_ocp_teamleader","rhsusf_spcs_ucp_teamleader","rhsusf_spcs_ucp",
"pbw_splitter_grpfhr","pbw_splitter_mg_h","pbw_splitter_mg","pbw_splitter_sani","pbw_splitter_schtz","pbw_splitter_zivil","V_RebreatherB","V_PlateCarrier2_rgr",
"V_PlateCarrier2_blk","V_PlateCarrier_Kerry","V_Press_F","U_B_FullGhillie_ard","U_B_FullGhillie_sard","U_B_FullGhillie_lsh","U_B_Wetsuit","U_B_T_Soldier_SL_F",
"U_B_CombatUniform_mcam_vest","U_B_PilotCoveralls","BWA3_Uniform_Crew_Tropen","BWA3_Uniform_Helipilot","BWA3_Uniform_Crew_Fleck","U_B_survival_uniform",
"U_B_T_FullGhillie_tna_F","U_C_HunterBody_grn","BWA3_Uniform_Ghillie_Tropen","BWA3_Uniform_Ghillie_Fleck","BWA3_Uniform_tee_Tropen",
"BWA3_Uniform_sleeves_Tropen","BWA3_Uniform_Tropen","BWA3_Uniform_tee_Fleck","BWA3_Uniform_sleeves_Fleck","BWA3_Uniform_Fleck","U_B_HeliPilotCoveralls",
"U_B_GhillieSuit","U_B_T_Sniper_F","PBW_Uniform4K_tropen","PBW_Uniform4_tropen","PBW_Uniform3K_tropen","PBW_Uniform3_tropen","PBW_Uniform4K_fleck",
"PBW_Uniform4_fleck","PBW_Uniform3K_fleck","PBW_Uniform3_fleck","PBW_Uniform2_tropen","PBW_Uniform1H_tropen","PBW_Uniform1_tropen","PBW_Uniform2_fleck",
"PBW_Uniform1H_fleck","PBW_Uniform1_fleck","BWA3_Uniform2_Ghillie_Tropen","BWA3_Uniform2_Ghillie_Fleck","BWA3_Uniform2_sleeves_Tropen","BWA3_Uniform2_Tropen",
"BWA3_Uniform2_sleeves_Fleck","BWA3_Uniform2_Fleck","rhsusf_ach_bare","rhsusf_ach_bare_des","rhsusf_ach_bare_des_ess","rhsusf_ach_bare_des_headset",
"rhsusf_ach_bare_des_headset_ess","rhsusf_ach_bare_ess","rhsusf_ach_bare_headset","rhsusf_ach_bare_headset_ess","rhsusf_ach_bare_semi",
"rhsusf_ach_bare_semi_ess","rhsusf_ach_bare_semi_headset","rhsusf_ach_bare_semi_headset_ess","rhsusf_ach_bare_tan","rhsusf_ach_bare_tan_ess",
"rhsusf_ach_bare_tan_headset","rhsusf_ach_bare_tan_headset_ess","rhsusf_ach_bare_wood","rhsusf_ach_bare_wood_ess","rhsusf_ach_bare_wood_headset",
"rhsusf_ach_bare_wood_headset_ess","rhsusf_ach_helmet_M81","rhsusf_ach_helmet_ocp","rhsusf_ach_helmet_ESS_ocp","rhsusf_ach_helmet_headset_ocp",
"rhsusf_ach_helmet_headset_ess_ocp","rhsusf_ach_helmet_camo_ocp","rhsusf_ach_helmet_ocp_norotos","rhsusf_ach_helmet_ucp","rhsusf_ach_helmet_ESS_ucp",
"rhsusf_ach_helmet_headset_ucp","rhsusf_ach_helmet_headset_ess_ucp","rhsusf_ach_helmet_ucp_norotos","rhsusf_cvc_green_helmet","rhsusf_cvc_green_alt_helmet",
"rhsusf_cvc_green_ess","rhsusf_cvc_helmet","rhsusf_cvc_alt_helmet","rhsusf_cvc_ess","PBW_b828_fleck","PBW_b828_tropen","PBW_barett_art","BWA3_Beret_Falli",
"PBW_barett_fespaeh","PBW_barett_fm","BWA3_Beret_HFlieger","BWA3_Beret_Jaeger","PBW_barett_ksk","PBW_barett_nsch","BWA3_Beret_PzAufkl","BWA3_Beret_PzGren",
"BWA3_Beret_Pz","PBW_barett_pi","PBW_barett_san","BWA3_Beret_Wach_blue","BWA3_Beret_Wach_green","PBW_muetze3_grau","rhsusf_Bowman","rhsusf_bowman_cap",
"PBW_Buschhut_fleck","BWA3_Booniehat_Fleck","PBW_Buschhut_tropen","H_Booniehat_tan","H_Booniehat_oli","BWA3_Booniehat_Tropen","rhs_xmas_antlers",
"H_HelmetCrew_B","BWA3_CrewmanKSK","BWA3_CrewmanKSK_Fleck","BWA3_CrewmanKSK_Fleck_Headset","BWA3_CrewmanKSK_Headset","BWA3_CrewmanKSK_Tropen",
"BWA3_CrewmanKSK_Tropen_Headset","rhsusf_opscore_aor1","rhsusf_opscore_aor1_pelt","rhsusf_opscore_aor1_pelt_nsw","rhsusf_opscore_aor2",
"rhsusf_opscore_aor2_pelt","rhsusf_opscore_aor2_pelt_nsw","rhsusf_opscore_bk","rhsusf_opscore_bk_pelt","rhsusf_opscore_coy_cover",
"rhsusf_opscore_coy_cover_pelt","rhsusf_opscore_fg","rhsusf_opscore_fg_pelt","rhsusf_opscore_fg_pelt_cam","rhsusf_opscore_fg_pelt_nsw",
"rhsusf_opscore_mc_cover","rhsusf_opscore_mc_cover_pelt","rhsusf_opscore_mc_cover_pelt_nsw","rhsusf_opscore_mc_cover_pelt_cam","rhsusf_opscore_mc",
"rhsusf_opscore_mc_pelt","rhsusf_opscore_mc_pelt_nsw","rhsusf_opscore_paint","rhsusf_opscore_paint_pelt","rhsusf_opscore_paint_pelt_nsw",
"rhsusf_opscore_paint_pelt_nsw_cam","rhsusf_opscore_rg_cover","rhsusf_opscore_rg_cover_pelt","rhsusf_opscore_ut","rhsusf_opscore_ut_pelt",
"rhsusf_opscore_ut_pelt_cam","rhsusf_opscore_ut_pelt_nsw","rhsusf_opscore_ut_pelt_nsw_cam","rhsusf_opscore_mar_fg","rhsusf_opscore_mar_fg_pelt",
"rhsusf_opscore_mar_ut","rhsusf_opscore_mar_ut_pelt","PBW_muetze1_fleck","PBW_muetze2_fleck","PBW_muetze1_tropen","PBW_muetze2_tropen","BWA3_M92_Fleck",
"BWA3_M92_Tropen","H_CrewHelmetHeli_B","H_PilotHelmetHeli_B","rhsusf_hgu56p_black","rhsusf_hgu56p_mask_black","rhsusf_hgu56p_mask_black_skull",
"rhsusf_hgu56p_visor_black","rhsusf_hgu56p_visor_mask_black","rhsusf_hgu56p_visor_mask_Empire_black","rhsusf_hgu56p_visor_mask_black_skull",
"rhsusf_hgu56p_green","rhsusf_hgu56p_mask_green","rhsusf_hgu56p_mask_green_mo","rhsusf_hgu56p_visor_green","rhsusf_hgu56p_visor_mask_green",
"rhsusf_hgu56p_visor_mask_green_mo","rhsusf_hgu56p","rhsusf_hgu56p_mask","rhsusf_hgu56p_mask_mo","rhsusf_hgu56p_mask_skull","rhsusf_hgu56p_visor",
"rhsusf_hgu56p_visor_mask","rhsusf_hgu56p_visor_mask_mo","rhsusf_hgu56p_visor_mask_skull","rhsusf_hgu56p_pink","rhsusf_hgu56p_mask_pink",
"rhsusf_hgu56p_visor_pink","rhsusf_hgu56p_visor_mask_pink","rhsusf_hgu56p_saf","rhsusf_hgu56p_mask_saf","rhsusf_hgu56p_visor_saf",
"rhsusf_hgu56p_visor_mask_saf","rhsusf_hgu56p_mask_smiley","rhsusf_hgu56p_visor_mask_smiley","rhsusf_hgu56p_usa","rhsusf_hgu56p_visor_usa",
"rhsusf_hgu56p_white","rhsusf_hgu56p_visor_white","rhsusf_ihadss","RHS_jetpilot_usaf","BWA3_Knighthelm","rhsusf_lwh_helmet_M1942",
"rhsusf_lwh_helmet_marpatd","rhsusf_lwh_helmet_marpatd_ess","rhsusf_lwh_helmet_marpatd_headset","rhsusf_lwh_helmet_marpatwd",
"rhsusf_lwh_helmet_marpatwd_blk_ess","rhsusf_lwh_helmet_marpatwd_headset_blk2","rhsusf_lwh_helmet_marpatwd_headset_blk","rhsusf_lwh_helmet_marpatwd_headset",
"rhsusf_lwh_helmet_marpatwd_ess","PBW_Helm4_fleck","PBW_Helm4_fleck_HBO","PBW_Helm4_fleck_HBOD","PBW_Helm5_fleck","PBW_Helm5_fleck_H","PBW_Helm4_fleck_H",
"PBW_Helm4_tropen","PBW_Helm4_tropen_HBO","PBW_Helm4_tropen_HBOD","PBW_Helm4_tropen_H","PBW_Helm6_fleck","PBW_Helm6_tropen","PBW_Helm1_fleck",
"PBW_Helm1_fleck_HBOD","PBW_Helm2_fleck","PBW_Helm2_fleck_H","PBW_Helm1_fleck_HBO","PBW_Helm1_fleck_H","PBW_Helm1_tropen","PBW_Helm1_tropen_HBO",
"PBW_Helm1_tropen_HBOD","PBW_Helm1_tropen_H","PBW_Helm3_fleck","PBW_Helm3_tropen","PBW_Helm3_UN","rhsusf_mich_bare","rhsusf_mich_bare_alt",
"rhsusf_mich_bare_headset","rhsusf_mich_bare_norotos","rhsusf_mich_bare_norotos_alt","rhsusf_mich_bare_norotos_alt_headset","rhsusf_mich_bare_norotos_arc",
"rhsusf_mich_bare_norotos_arc_alt","rhsusf_mich_bare_norotos_arc_alt_headset","rhsusf_mich_bare_norotos_arc_headset","rhsusf_mich_bare_norotos_headset",
"rhsusf_mich_bare_semi","rhsusf_mich_bare_alt_semi","rhsusf_mich_bare_semi_headset","rhsusf_mich_bare_norotos_semi","rhsusf_mich_bare_norotos_alt_semi",
"rhsusf_mich_bare_norotos_alt_semi_headset","rhsusf_mich_bare_norotos_arc_semi","rhsusf_mich_bare_norotos_arc_alt_semi",
"rhsusf_mich_bare_norotos_arc_alt_semi_headset","rhsusf_mich_bare_norotos_arc_semi_headset","rhsusf_mich_bare_norotos_semi_headset",
"rhsusf_mich_bare_tan","rhsusf_mich_bare_alt_tan","rhsusf_mich_bare_tan_headset","rhsusf_mich_bare_norotos_tan","rhsusf_mich_bare_norotos_alt_tan",
"rhsusf_mich_bare_norotos_alt_tan_headset","rhsusf_mich_bare_norotos_arc_tan","rhsusf_mich_bare_norotos_arc_alt_tan",
"rhsusf_mich_bare_norotos_arc_alt_tan_headset","rhsusf_mich_bare_norotos_tan_headset","rhsusf_mich_helmet_marpatd","rhsusf_mich_helmet_marpatd_alt",
"rhsusf_mich_helmet_marpatd_alt_headset","rhsusf_mich_helmet_marpatd_headset","rhsusf_mich_helmet_marpatd_norotos","rhsusf_mich_helmet_marpatd_norotos_arc",
"rhsusf_mich_helmet_marpatd_norotos_arc_headset","rhsusf_mich_helmet_marpatd_norotos_headset","rhsusf_mich_helmet_marpatwd","rhsusf_mich_helmet_marpatwd_alt",
"rhsusf_mich_helmet_marpatwd_alt_headset","rhsusf_mich_helmet_marpatwd_headset","rhsusf_mich_helmet_marpatwd_norotos","rhsusf_mich_helmet_marpatwd_norotos_arc",
"rhsusf_mich_helmet_marpatwd_norotos_arc_headset","rhsusf_mich_helmet_marpatwd_norotos_headset","BWA3_OpsCore","BWA3_OpsCore_Fleck","BWA3_OpsCore_Fleck_Camera",
"BWA3_OpsCore_Fleck_Patch","BWA3_OpsCore_Camera","BWA3_OpsCore_Tropen","BWA3_OpsCore_Tropen_Camera","BWA3_OpsCore_Tropen_Patch","rhsusf_patrolcap_ocp",
"rhsusf_patrolcap_ucp","H_PilotHelmetFighter_B","H_Hat_Safari_olive_F","H_Hat_Safari_sand_F","H_ShemagOpen_khk","H_Cap_headphones","H_ShemagOpen_tan",
"H_Shemag_olive","H_Shemag_olive_hs","H_HelmetO_ViperSP_hex_F","H_HelmetO_ViperSP_ghex_F"]
	
	
	]] call ace_arsenal_fnc_initBox;*/
private ["_backpackTypeAndItems","_backpack"];
_backpackTypeAndItems = [(backpack player),(backpackItems player)];
player setVariable ["backpackTypeAndItems",_backpackTypeAndItems];
_backpack = createVehicle ["groundWeaponHolder", (getPos player) , [], 0, "can_collide"];
_backpack addBackpackCargoGlobal [(backpack player), 1];
_backpack attachTo [player,[-0.1,0.75,-0.4],"pelvis"];
_backpack setVectorDirAndUp [[0,0,-1],[0,1,0]];
player setVariable ["backpack_type",_backpack];
player setVariable ["backpack_anim",(animationState player)];
removebackpackglobal player;
player forceWalk true;

[player,1,["ACTION_BACKBACK_CHEST",localize "str_backpack_chest"]] call ace_interact_menu_fnc_removeActionFromObject;

private "_ACE_Action";

_ACE_Action = ["ACTION_BACKBACK_BACK", localize "str_backpack_back", "\A3\Air_F_Beta\Parachute_01\Data\UI\Portrait_Parachute_01_CA.paa", {[true] call compile preprocessFileLineNumbers "scripts\client\player\backpack_to_back.sqf";}, {((backpack player) isEqualTo "")and(!(isNull (player getVariable ["backpack_type",objNull])))}] call ace_interact_menu_fnc_createAction;
[player, 1, ["ACE_SelfActions", "ACE_Equipment"], _ACE_Action] call ace_interact_menu_fnc_addActionToObject;
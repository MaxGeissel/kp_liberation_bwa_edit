private ["_backpack"];
private _backpack = player getVariable ["backpack_type",objNull];
_isParachute = ((vehicle player) isKindOf "ParachuteBase");
if (_isParachute) then {
	_backpack attachTo [(vehicle player),[-0.08,0.55,-0.42]];
	_backpack setVectorDirAndUp [[0,-0.2,-1],[0,1,0]];
} else {
	detach _backpack;
	_backpack setPos [random 50,random 50,(10000 + (random 50))];
};

_isHalo = (("halofreefall" in ((animationState player) splitString "_"))||((stance player) isEqualTo "PRONE"));
if ((_isHalo)) then {
	_backpack attachTo [player,[-0.1,-0.4,-0.75],"pelvis"];
	_backpack setVectorDirandup [[0,-1,0],[0,0,-1]];
}else{
	_backpack attachTo [player,[-0.1,0.75,-0.4],"pelvis"];
	_backpack setVectorDirAndUp [[0,0,-1],[0,1,0]];
};
private["_backpackTypeAndItems","_backpack"];
_backpackTypeAndItems = player getVariable ["backpackTypeAndItems",[]];
If (!(_backpackTypeAndItems isEqualTo [])) then {
	player addBackpackGlobal (_backpackTypeAndItems select 0);
	{
		player addItemToBackpack _x;
	}forEach (_backpackTypeAndItems select 1);
	player setVariable ["backpackTypeAndItems",[]];
};
_backpack = player getVariable ["backpack_type",objNull];
If (!(isNull _backpack)) then {
	clearBackpackCargoGlobal _backpack;
	detach _backpack;
	deleteVehicle _backpack;
	player setVariable ["backpack_type",objNull];
	player forceWalk false;

	[player,1,["ACTION_BACKBACK_BACK",localize "str_backpack_back"]] call ace_interact_menu_fnc_removeActionFromObject;

	private "_ACE_Action";
	_ACE_Action = ["ACTION_BACKBACK_CHEST", localize "str_backpack_chest", "\A3\Air_F_Beta\Parachute_01\Data\UI\Portrait_Parachute_01_CA.paa", {[true] call compile preprocessFileLineNumbers "scripts\client\player\backpack_to_chest.sqf";}, {(!((backpack player) isEqualTo ""))and(isNull (player getVariable ["backpack_type",objNull]))}] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "ACE_Equipment"], _ACE_Action] call ace_interact_menu_fnc_addActionToObject;
};

[] call compileFinal preprocessFileLineNumbers "whitelist.sqf";

if ( !GRLIB_use_whitelist ) exitWith {};

waitUntil {alive player};
sleep 1;

// TOC Slots
if (((str player) in KPLIB_BWA_TOC_slots ) && !((getPlayerUID player) in BWA_TOC_id)) then {
	endMission "ENDTOC";
};

if (((str player) == "commandant") && !((getPlayerUID player) in GRLIB_whitelisted_steamids || ( getPlayerUID player) in BWA_stammspieler_steamids)) then {
	endMission "END1";
};

if (((str player) == "stelv_commandant") && !((getPlayerUID player) in BWA_stammspieler_steamids || (getPlayerUID player) in GRLIB_whitelisted_steamids)) then {
	endMission "END1";
};

if (((str player) in KPLIB_BWA_zugfuehrung_slots ) && !((getPlayerUID player) in BWA_stammgast_steamids || (getPlayerUID player) in BWA_stammspieler_steamids || (getPlayerUID player) in GRLIB_whitelisted_steamids)) then {
	endMission "END2";
};

// Wenn Spieler auf Pilotenslot und kein Recht hat zur Nutzung von Luftfahrzeugen (Liberation Rechte System), dann zurück zur Lobby
if (((str player) in KPLIB_BWA_jetpilot) && !([player, 2] call F_fetchPermission)) then {
	endMission "END3";
};


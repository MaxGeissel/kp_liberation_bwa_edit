//createt by GRAKA

if (!isServer) exitWith {};

// Postinit oder Lange Warten
sleep 60;

// Setowner fuer Slingloading (stabilere Frachthaken, Glitchfix)
/*
CBA Funktion (Eventhandler für alle Helis)
Setowner Funktioniert auch mit Passagieren (aussgenommen Fahrer)
allerdings nur mit RPT Error (siehe BI Bugtracker), 
vorerst also nur Bei Kisten und Leeren Fahrzeugen Aktiviert
*/
["Helicopter", "init", {
    params ["_heli"];

    _heli addEventHandler ["RopeAttach", {
        params ["_heli", "", "_cargo"];

        if ((fullCrew _cargo) isEqualTo []) then {
            private _heliOwner = owner _heli;
			
            if (_heliOwner != owner _cargo) then {
                _cargo setOwner _heliOwner;
            };
        };
    }];
}, true, [], true] call CBA_fnc_addClassEventHandler;

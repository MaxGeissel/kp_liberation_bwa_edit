if (!isServer) exitWith {};

{
	switch (getPlayerUid _x) do {
		case "76561198046261212": {
			if !((getAssignedCuratorLogic _x) isEqualTo zm_evil) then {
				unassignCurator (getAssignedCuratorLogic _x);
				unassignCurator zm_evil;
				sleep 2;
				_x assignCurator zm_evil;
			}
		};
		case "76561198142842785": {
			if !((getAssignedCuratorLogic _x) isEqualTo zm_max) then {
				unassignCurator (getAssignedCuratorLogic _x);
				unassignCurator zm_max;
				sleep 2;
				_x assignCurator zm_max;
			}
		};
		case "76561197973051580": {
			if !((getAssignedCuratorLogic _x) isEqualTo zm_lexel) then {
				unassignCurator (getAssignedCuratorLogic _x);
				unassignCurator zm_lexel;
				sleep 2;
				_x assignCurator zm_lexel;
			}
		};
		case "76561198097683449": {
			if !((getAssignedCuratorLogic _x) isEqualTo zm_chaser) then {
				unassignCurator (getAssignedCuratorLogic _x);
				unassignCurator zm_chaser;
				sleep 2;
				_x assignCurator zm_chaser;
			}
		};
		default {
			if ((str player) == "commandant") then {
				if (isNull(getAssignedCuratorLogic commandant)) then {
					unassignCurator zm1;
					sleep 2;
					commandant assignCurator zm1;
				};
			};
		};
	};
} forEach allPlayers;
